let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
const sanitizer = require('sanitize')();

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', socket => {
    socket.on('message', function(args) {
        io.emit('message', { ...sanitizer.primitives(args), time: new Date().toUTCString() });
    });
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});
